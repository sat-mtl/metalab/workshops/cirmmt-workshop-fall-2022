# Presenters

Edu Meneses is a music technologist, digital luthier, composer, and performer in the best interdisciplinary fashion.
A Ph.D. in music technology at McGill University (IDMIL and CIRMMT), Edu is currently a researcher-developer at SAT. 
He works with embedded systems for artistic tools and sound spatialization. 

Michał Seta is a composer, improviser, and digital arts researcher. 
Practitioner of transdisciplinary, transcalar, and integrative magic, he incants Metalab software in collective and improvised harmony. 
He is one of the main contributors to the SATIE software.