# Metalab workshop proposal

## Workshop abstract

This workshop is aimed at art researchers, artists, designers, content creators, and other creatives interested in creating immersive spaces using research-developed tools.
We will use off-the-shelf hardware and open-source software to explore SAT/Metalab tools, connecting them with the existing workflow of researchers and artists.
Participants will learn about Metalab tools for different artistic tasks, including sound spatialization, video mapping, and computer vision.
The topics include evaluating the available space, setting up projection mapping, calibrating video projectors and sound equipment, setting up cameras for live pose tracking and estimation, and employing a software pipeline to map everything together.

## Target audience

The CIRMMT community (members, collaborators, and students) is the target audience for this workshop.
However, participation can be extended to the artistic community interested in digital arts.
We propose 5 active participants, that will receive support to interact with the presented tools, including mapping their software/equipment of choice into Metalab tools using predefined methods: direct audio connection, JackTrip, OSC, and NDI.
The required information will be provided to the active participants before the workshop.

We also propose to the remaining interested participants attend as listeners.

## Software/hardware information

We will explore production pipelines to illustrate the use of the chosen software in tandem with existing tools that have been seeing broader adoption in recent years.
For clarity, we present the proposed toolset in two groups: software developed by Metalab and other software.

- Metalab software:
  - [LivePose](https://gitlab.com/sat-mtl/metalab/livepose): A computer-vision tool for real-time skeleton tracking from an RGB or grayscale video feed (live or not). LivePose applies various filters (for detection, selection, improving the data, etc.) and sends the results through the network (OSC and Websocket are currently supported).
  - [Splash](https://gitlab.com/sat-mtl/metalab/splash): A modular mapping software that uses 3D models with UV mapping of the projection surface to calibrate multiple video projectors (intrinsic and extrinsic parameters, blending, and color), and feed them with the input video sources.
  - [SATIE](https://gitlab.com/sat-mtl/metalab/satie): An audio spatialization engine developed for real-time rendering of dense audio scenes to large multi-channel loudspeaker systems. SATIE is capable of outputting multiple sound formats and speaker setups simultaneously. There is no geometry per se in SATIE. Rather, SATIE maintains a DSP graph of source nodes accumulated to a single "listener," corresponding to the renderer's output configuration (stereo and/or multi-channel).
  - [Shmdata](https://gitlab.com/sat-mtl/metalab/shmdata): A library to share flows of data frames between processes via shared memory.
- Other software and transmission protocols:
  - [libmapper/webmapper](https://github.com/libmapper/libmapper): A software library for declaring data signals on a shared network and enabling arbitrary connections between them. libmapper was initially developed at IDMIL/CIRMMT and is currently maintained between McGill, Dalhousie University, and collaborators, including Metalab.
  - [OSC](https://ccrma.stanford.edu/groups/osc/index.html): A data transport specification for network messaging between applications and hardware.

Most of the above software can run in most Linux-based systems, including low-cost, low-energy hardware such as Raspberry Pi and NVIDIA Jetson single-board computers.
Participants can install the Metalab tools in their systems or use embedded systems (provided by Metalab) containing those tools.

## Duration and procedure

- 1-day event
  - Start: 8h45
  - End: 16h30
- Proposed timeline
  - **08h45** - Welcoming
  - **09h15** - Presentation of Metalab tools: integration, interoperability, API, documentation, etc.
  - **09h30** - Introduction of the workshop and introduction of each participant
  - **10h30** - Coffee break
  - **11h00** - Work period 1: setting and connecting tools
  - **12h00** - Lunch
  - **13h00** - Work period 2: directed collective creation of an immersive space
  - **15h00** - Coffee break
  - **15h30** - Immersive space finalization and deployment
  - **16h00** - Closing remarks

## About Metalab

Founded in 2002, Metalab is the research laboratory of the Society for Arts and Technology [SAT]. Metalab's missions are to stimulate the emergence of innovative immersive experiences and to make their design accessible to artists and art creators.
Metalab accomplishes these missions through an ecosystem of free software that addresses problems not easily solved by existing tools.
The software and hardware developed at Metalab are often used with other technologies to facilitate mixing video mapping, 3D audio, telepresence, and interaction.

## List of Equipment

- **provided by Metalab:**
  - Raspberry Pi
  - Jetson Xavier NX
  - audio interface(s)
  - portable projectors
  - cameras
  - cables
- **provided by CIRMMT:**
  - if needed: extra audio interface(s) (USB audio class compliant for compatibility with Linux systems)
  - speakers and associated audio cables
  - presenter's projector, appropriate for the workshop venue
  - headphones
